About & License
===============

The Startup till Shutdown Synchronization Protocol (SSSP) defines a standard for
synchronous, real-time capable startup, operation and shutdown of distributed
systems. Originally developed for the Autonomous Mini Robot (AMiRo) [1], it can
also be used for other platforms, as its requirements are very low.

Copyright (C) 2016..2022 Thomas Schöpping et al. (a complete list of all authors
is given below)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.

This research/work was supported by the Cluster of Excellence Cognitive
Interaction Technology 'CITEC' (EXC 277) at Bielefeld University, which is
funded by the German Research Foundation (DFG).

Authors
-------

- Thomas Schöpping (tschoepp[at]cit-ec.uni-bielefeld.de)

References
----------

**[1]** S. Herbrechtsmeier, T. Korthals, T. Schopping and U. Rückert, "AMiRo: A modular & customizable open-source mini robot platform," 2016 20th International Conference on System Theory, Control and Computing (ICSTCC), Sinaia, 2016, pp. 687-692.

--------------------------------------------------------------------------------

Contents
========

1. [Purpose and Goals of SSSP](#purpose-and-goals-of-sssp)
2. [Version History](#version-history)
   1. [SSSP 2.x](#sssp-2x)
      1. [SSSP 2.0 (draft)](#sssp-20-draft)
   2. [SSSP 1.x](#sssp-1x)
      1. [SSSP 1.4](#sssp-14)
      2. [SSSP 1.3](#sssp-13)
      3. [SSSP 1.2](#sssp-12)
      4. [SSSP 1.1](#sssp-11)
      5. [SSSP 1.0](#sssp-10)

--------------------------------------------------------------------------------

Purpose and Goals of SSSP
=========================

The Startup till Shutdown Synchronization Protocol (SSSP) defines a standard for synchronous, real-time capable startup, operation and shutdown of distributed systems, comprising all relevant components, like bootloader and operating system.
At the same time, complexity and system requirements are kept on a minimal level.
SSSP defines signal handling during the startup phase until all modules are fully initialized, and during the shutdown phase so that the system turns off in a controlled and safe manner or restarts, if requested.
The complexity of the protocol is quite low and designed in a way that modules which do not implement SSSP will not compromise system operation.

The goal of SSSP is to achieve full real-time capabilities for distributed systems where the individual modules may host any digital logic, such a low-power microcontrollers, high-performance SoCs or even FPGAs.
Whitelist the minimum requirements are very low, additional features may demand for slightly advanced features.
However, the protocol is designed in a way, that setups in which some modules do not implement all features of SSSP (or no SSSP at all) will not compromise system operation, even though some features of SSSP might not be available in such cases.



Version History
===============

There exist several major and minor versions of SSSP.
This section lists all versions in descending order.

SSSP 2.x
--------

### [SSSP 2.0](./version_2-x/version_2-0/SSSP_2-0.md) (draft)
- Redefinition of signal properties, causing incompatibility with versions [1.x](./version_1-x/).
- Introduced an optional clock signal `C` that can be used for system synchronization during operation phase instead of `S`.
- Due to multiple usage of the parameter `T`, it was divided into two individual ones `D` and `T`.
- Introduced an additional optional step during the startup phase, which allows to rearrange module identifiers if desired.


SSSP 1.x
--------

### [SSSP 1.4](./version_1-x/version_1-4/SSSP_1-4.md)
- Optimized stage 3 of the startup phase (module stack initialization).
  The procedure can now be aborted by any module and instead of the master, the final module terminates the stage.
- Removed the constraint that the largest possible module identifier (e.g. 255 for 8-bit addressing) was reserved.
  Thus, only a value of 0 is reserved, which indicates unavailable module IDs and can be used as broadcast address in any case (identifiers may or may not be available).

### [SSSP 1.3](./version_1-x/version_1-3/SSSP_1-3.md)
- Introduced the Operation Phase and the additional parameter `F`.
  The new phase allows for precise temporal synchronization of all modules.

### [SSSP 1.2](./version_1-x/version_1-2/SSSP_1-2.md)
- Introduced the parameter `T` for adjustable delays.
  Before, `T` was predefined as one millisecond.

### [SSSP 1.1](./version_1-x/version_1-1/SSSP_1-1.md)
- Introduced the module stack initialization (stage 3 of the startup sequence).
  This feature allows to dynamically count the number of modules and use the order of the stack as hierarchy for later communication.
- Introduced the disambiguation procedure in the shutdown phase (stage 2.2).
  This way it is now possible to trigger all shutdown/restart procedures from any module.

### [SSSP 1.0](./version_1-x/version_1-0/SSSP_1-0.md)
- Initial version of SSSP.

