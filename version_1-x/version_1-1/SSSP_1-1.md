About & License
===============

The Startup till Shutdown Synchronization Protocol (SSSP) defines a standard for
synchronous, real-time capable startup, operation and shutdown of distributed
systems. Originally developed for the Autonomous Mini Robot (AMiRo) [1], it can
also be used for other platforms, as its requirements are very low.

Copyright (C) 2016..2022 Thomas Schöpping et al. (a complete list of all authors
is given below)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.

This research/work was supported by the Cluster of Excellence Cognitive
Interaction Technology 'CITEC' (EXC 277) at Bielefeld University, which is
funded by the German Research Foundation (DFG).

Authors
-------

- Thomas Schöpping (tschoepp[at]cit-ec.uni-bielefeld.de)

References
----------

**[1]** S. Herbrechtsmeier, T. Korthals, T. Schopping and U. Rückert, "AMiRo: A modular & customizable open-source mini robot platform," 2016 20th International Conference on System Theory, Control and Computing (ICSTCC), Sinaia, 2016, pp. 687-692.

--------------------------------------------------------------------------------

Contents
========

1. [SSSP 1.1](#sssp-11)
    1. [Startup Phase](#startup-phase)
    2. [Shutdown Phase](#shutdown-phase)

--------------------------------------------------------------------------------

SSSP 1.1
========

The Startup till Shutdown Synchronization Protocol (SSSP) defines signal handling during the startup phase until all modules are fully initialized, and during the shutdown phase so that the system turns off in a controlled and safe manner or restarts, if requested.
The complexity of the protocol is quite low and designed in a way that modules which do not implement SSSP will not compromise system operation.
Hence, only two signals are required:

- `S` - *synchronize*
- `PD` - *power down*

Both must be designed in a way, that they realize a logical *OR* on activation (one or more nodes are active) and a logical *AND* on deactivation (all nodes are inactive) respectively.
Electrically this can be implemented using active-low open-drain signals with pull-up resistors.


Startup Phase
-------------

All modules must initialize the signals in a way, that `S` is active and `PD` is inactive.
Although only `S` is used for startup, `PD` must be inactive during the startup phase, or the shutdown phase will be initiated immediately by either the bootloader or the operating system as soon as it is active.

In the startup phase, each module executes the following steps:

1. **basic initialization**
    1. **initialization of required signals, voltages, and other hardware**<br>
        When a module has finished this stage, it sets `S` to inactive.
        In order to prevent erroneous behavior due to incorrect signals during the initialization, this stage takes at least one millisecond.
    2. **waiting for synchronization**<br>
        Each module waits for `S` to become inactive (all modules are initialized) as a first synchronization.
    3. **synchronous start of stage 2**<br>
        As soon as `S` is inactive, the master node activates it again in order to start the next stage.
        To ensure that each module had enough time to detect the inactive state of `S`, the master node must delay the activation by at least one millisecond.
2. **operating system initialization**
    1. **complete system startup**<br>
        Each module activates `S` again and fully initializes (e.g. starts the operating system).
        As soon as it is ready, it deactivates `S` again.
        When a module indicates to be ready, at least the main communication channel (for AMiRo this is CAN) must be fully operational.
        Again, `S` must be active for at least one millisecond, so every module can detect the activation.
    2. **waiting for synchronization**<br>
        Each module waits for `S` to become inactive (all modules are ready).
        Only now it is safe to use the main communication channel and all modules are able to receive messages correctly.
3. **module stack initialization**<br>
    This stage can only be applied if all modules can read and write from/to the main communication channel and two additional signals, which connect neighboring modules, exist.
    Furthermore, the first and last node of this 'module stack' must be known beforehand.
    In case of the AMiRo, for instance, the *DiWheelDrive* and *PowerManagement* are defined to be the lowermost modules, and the *LightRing* always finalizes the stack at the top.
    1. **initiation of this stage**<br>
        The master node initiates this stage by broadcasting an unique command to all modules, so they can interpret the upcoming communication via the neighbor signals correctly.
    2. **starting the sequence**<br>
        One of the known nodes at the end of the module stack broadcasts its own stack number (e.g. 1) via the bus.
        Right after that, it signals its neighboring module to continue by setting the neighbor signal active for at least one millisecond.
        Note that the smallest and largest numbers (0 and 255 for 8-bit addressing) are reserved and must not be used by any module.
        Furthermore, these 'stack IDs' are defined to represent an hierarchy, so that lower IDs are dominant to larger ones.
    3. **counting the modules**<br>
        When a module is triggered by the activation of the neighbor signal, it broadcasts its own stack number, which is defined to be greater than the last one.
        Then again, it triggers the next module in the stack to continue via the other neighbor signal.
        This step is repeated until one of the two terminating conditions is fulfilled (see next step).
    4. **termination of this stage**<br>
        There are two ways how this stage can be terminated: either it is completed correctly, or it is aborted because of a timeout.
        - **completion**<br>
          The stage is completed correctly if the signal is propagated to the known node on the other end of the module stack.
            1. **broadcast of final ID**<br>
                The final (known) module broadcasts its own stack number (which is computed as the others before) plus the information, that the module stack initialization is done.
            2. **successful termination of the stage**<br>
                The master node broadcasts a messages, which indicates the successful termination of the stage. In this case, all nodes adopt their ID and can use it for later identification.
        - **abortion**<br>
          The stage is aborted, if more than ten milliseconds have passed since the last ID was broadcasted.
          Such a timeout can only occur if a module does not support the propagation process, or because of hardware issues.
          When the master node detects such a delay, it broadcasts a message, which indicates the unsuccessful termination of the stage.
          As result, all stack numbers must be considered as unreliable in future communication, thus identification is not supported.

At the end of the startup phase (more precisely after stage 2), both signals, `S` and `PD`, are inactive.
Note that a module, which does not implement the protocol, will not interfere and cause no errors as long as it does not activate `S`.
However, such a module might cause errors after the startup phase, if it does not receive crucial information because communication is not set up (e.g. stage 3 may fail).


Shutdown Phase
--------------

Since the two signals `S` and `PD` must not be used during system operation, both are defined to be inactive.
Any module can initiate the shutdown phase by activation of `PD`.
All modules (including the initiating one) must then execute the following steps as soon as the activation of `PD` is detected:

1. **shutdown of high-level operation**
    1. **initiation of module shutdown**<br>
        As soon as the activation of `PD` is detected, each module activates `S`.
        The module, which initiated system shutdown by activating `PD` has to activate `S` as well, of course.
    2. **shutdown of high-level operation** (e.g. the operating system)<br>
        Each module stops all computation in a safe manner, so it can be shut down without data loss or other issues.
        As soon as this is done, it deactivates `S`.
        In order to ensure that every module had a chance to detect the activation of `PD`, this step must take at least one millisecond.
    3. **waiting for synchronization**<br>
        Each module waits for `S` to become inactive (all modules are done).
2. **system shutdown or restart**
    1. **evaluation of `PD` signal**<br>
        When `S` becomes inactive, the state of `PD` indicates whether the system shall shut down or restart.
        Hence, the initiating module, which activated `PD`, must have set it to the according state before it deactivated `S`.
        The implication of the `PD` state at this point is defined as follows:
        - active: A system shutdown is requested.
        - inactive: A system restart is requested.
    2. **disambiguation procedure**<br>
        Since there may be not one, but multiple ways to shutdown/restart the system, this ambiguity is resolved in the following procedure.
        The requirement for this to work is that the identifiers, which encode the exact shutdown/restart procedure to be executed (see below), must be non-ambiguous.
        These identifiers, however, dependent on the platform and implementation and hence are not defined by SSSP.
        1. **serial broadcast of identifier**<br>
            The module which initiated the shutdown/restart phase broadcasts an arbitrary number of 'pulses' via `S`.
            Each 'pulse' is defined to start with `S` deactivated, activates it for at least one millisecond, and deactivates the signal again for at least one more millisecond.
            All modules can count the number of pulses, which encodes the exact shutdown/restart procedure to be used.
            Note that `S` must be inactive for at least one millisecond before the first pulse (after `PD` was evaluated).
        2. **termination of the serial broadcast**<br>
            The broadcast is terminated by a timeout of ten milliseconds since the last change of `S` from active to inactive state.
            This timeout also applies if no pulse was sent at all, which corresponds to the identifier 0.
            Thus, this identifier is reserved for the special case, that the ambiguity is not resolved and all modules shall execute their default shutdown procedure.
    3. **final shutdown or restart**<br>
        Depending on the evaluation of `PD` and the result of the disambiguation procedure, each module reacts accordingly.
        - **shutdown**<br>
          Each module completely stops itself and enters low-power mode.
          The details (e.g. which signals and sensors are still active) depend on the result of the disambiguation procedure.
        - **restart**<br>
          If a restart was requested, each module starts with the first step of the startup phase.
          The details (e.g. which sensors are kept active) depend on the result of the disambiguation procedure.
          In order to minimize risk of errors, all modules can power off, except for a master node, which resets the whole system and forces a clean startup.

Again, a module which does not implement the protocol will cause no errors as long as it does not activate `S` or `PD`.
However, if such a module has its own power supply and does not enter low-power mode, it will unnecessarily draw energy and might not end up in a defined state as the rest of the system.

